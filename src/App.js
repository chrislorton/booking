import React from 'react';
import './styles/styles.scss';
import Home from './components/Home';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

export default function App() { 

    return (
        <Router>
            <main id="app">
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                </Switch>
            </main>
        </Router>
    )
}