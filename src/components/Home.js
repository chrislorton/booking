import React, { useState } from 'react';
import axios from "axios"

export default function Home() {

    const [searchText, setSearchText] = useState(null);
    const [showSearchResults, setShowSearchResults] = useState(false);
    const [searchResults, setSearchResults] = useState([]);


    const handleSearchInput = (e) => {
        setSearchText(e.target.value)
        if(e.target.value.length > 1){
            setShowSearchResults(true)
            makeSearchRequest(e.target.value)
        } else {
            setShowSearchResults(false)
        }
    }

    const makeSearchRequest = (str) => {
        axios.get(`https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=${str}`)
            .then(res => {
                if(res.data.results.numFound > 0){
                    setSearchResults(res.data.results.docs)
                } else {
                    setSearchResults([])
                }
            })
    }

    return (
        <div className="cb-u-background-primary--dark cb-c-hero-background">
            <div id="searchbox">
                <form className="c-searchbox cb-u-clearfix">
                    <h2 className="c-searchbox__title cb-u-color-greyscale--dark">
                        Let’s find your ideal car
                    </h2>
                    <div className="c-fts">
                        <label 
                            id="pickup-label"
                            name="pickup-label"
                            htmlFor="fts-pickupLocation" 
                            className="c-fts__label ui-clyde">
                            Pick-up Location
                        </label>
                        <div className="c-fts-input-container">
                            <input 
                                aria-labelledby="pickup-label"
                                type="text" 
                                id="fts-pickupLocation" 
                                name="fts-pickupLocation" 
                                autoComplete="off" 
                                className="ui-clyde c-fts-input cb-c-input" 
                                placeholder="city, airport, station, region, district…" 
                                required
                                onChange={handleSearchInput}/>
                        </div>
                    </div>
                    {(() => {
                        if(showSearchResults){
                            return <div    
                                className="c-fts-results" 
                                aria-live="assertive">
                                {(() => {
                                    if(searchResults.length === 0){
                                        return <span>No results found</span>
                                    } else {
                                        return searchResults.map((res, key) => {
                                            return <ol 
                                                key={key}
                                                id="fts-pickupLocationResults" 
                                                role="listbox" 
                                                className="c-fts-results__list cb-o-list-bare cb-u-margin-bottom-none">
                                                <li 
                                                    id="fts-pickupLocationResultsItem-0" 
                                                    className="cb-o-list-bare__item c-fts-results__item c-fts-results__item--focus" 
                                                    role="option" 
                                                    tabIndex="-1">
                                                    <div className="c-fts-results__pill-container">
                                                        <div className="cb-o-pill ui-tweed-bold c-fts-results__pill cb-u-margin-top-tiny">
                                                            {res.placeType}
                                                        </div>
                                                    </div>
                                                    <div className="c-fts-results__location-container">
                                                        <div className="ui-clyde c-fts-results__location-name">
                                                            <span>
                                                                <span>{res.name}</span>
                                                            </span> ({res.iata})
                                                        </div>
                                                        <div className="ui-tweed c-fts-results__support-text">
                                                            {res.city}, {res.country}
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        })
                                    }
                                })()}
                            </div>
                        }
                    })()}
                </form> 
            </div>
        </div>
    )
}